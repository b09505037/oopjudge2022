# Background
    以這次的期末報告作為契機，透過這次的機會得以學習讀取JSON檔案的資料，並設計GUI介面和創建資料庫以做出一個外送軟體，並進一步和地圖API結合。
    
# Install
> JSON：
>
> > 依照此教學設置JSON: https://www.youtube.com/watch?v=4rBeFDnw_oo&t=231s
>
> jxBroswer: https://jxbrowser-support.teamdev.com/docs/guides/installation/classpath.html
>
> > https://www.teamdev.com/jxbrowser (需要註冊)
>
> Sqlite: https://www.sqlite.org/download.html
>
> > https://www.sqlite.org/2022/sqlite-dll-win64-x64-3380500.zip
> > https://www.sqlite.org/2022/sqlite-tools-win32-x86-3380500.zip
>
> DB Browser for SQLite: https://sqlitebrowser.org/dl/
>
> > https://download.sqlitebrowser.org/DB.Browser.for.SQLite-3.12.2-win64.msi

# Usage
> 客人下單之後，訂單存至資料庫
> 商家和外送員確認訂單，更新位於資料庫的訂單狀態
> 送貨完成後，外送員更新訂單狀態

# Login
> store（account, password）
>
> > (～, 00000000) // 所有店家密碼皆相同
>
> member (account, password）
>
> > (新竹彭于晏, 11111111) (台中王嘉爾, 11111111)
>
> courier (account, password）
>
> > (綱哥, 22222222) (小綱, 22222222) 


# Maintainers
b09505013   高孝傑

b09505021   張景華

b09505037   林易陞
    

# order 
## 參數
 orderID
 
 price
 
 deliveryFee
 
 timeOfOrderCreated
 
 timeOfArrival
 
 deliveryLocation
 
 consumerName
 
 deliveryPersonName
 
 store
 
 info

# Consumer 
## 參數
 name
 
 pay
 
 password
 
 phoneNumber
 
 payDate
 
 lat
 
 lng

# Store 
## 參數
 name
 
 price
 
 password
 
 timeOfOrderCreated
 
 timeOfArrival
 
 deliveryLocation
 
 consumerName
 
 deliveryPersonName
 
 store
 
 info
  
# Crusier 
## 參數
 orderID
 
 price
 
 deliveryFee
 
 timeOfOrderCreated
 
 timeOfArrival
 
 deliveryLocation
 
 consumerName
 
 deliveryPersonName
 
 store
 
 info
