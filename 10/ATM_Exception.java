public class ATM_Exception extends Exception {

	public enum ExceptionTYPE {
		BALANCE_NOT_ENOUGH, AMOUNT_INVALID
	}

	private ExceptionTYPE err;

	/**
	 * check the error from balanceNotEnough or balanceNotEnough
	 * 
	 * @param err exception condition
	 */
	public ATM_Exception(ExceptionTYPE exceptionCondition) {
		if (exceptionCondition == ExceptionTYPE.BALANCE_NOT_ENOUGH) {
			err = ExceptionTYPE.BALANCE_NOT_ENOUGH;
		}
		if (exceptionCondition == ExceptionTYPE.AMOUNT_INVALID) {
			err = ExceptionTYPE.AMOUNT_INVALID;
		}
	}

	/**
	 * get message about error
	 * 
	 * @return error message
	 */
	public String getMessage() {
		String meg = null;
		switch (err) {
		case BALANCE_NOT_ENOUGH:
			meg = "BALANCE_NOT_ENOUGH";
			break;
		case AMOUNT_INVALID:
			meg = "AMOUNT_INVALID";
			break;
		}
		return meg;
	}

}
