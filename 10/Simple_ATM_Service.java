
/**
 * withdraw from ATM if having enough balance in account
 * 
 * @author linyisheng
 *
 */
public class Simple_ATM_Service implements ATM_Service {
	
	

	@Override
	/**
	 * check balance in accounts
	 * 
	 * @param account the user's account
	 * @param money   balance in account
	 * @return return true if having enough money; return false if not
	 * @throws ATM_Exception if not having enough money, print "BALANCE_NOT_ENOUGH"
	 */
	public boolean checkBalance(Account account, int money) throws ATM_Exception {
		

		int bal = account.getBalance() - money;
		if (bal <= 0) {
			throw new ATM_Exception(ATM_Exception.ExceptionTYPE.BALANCE_NOT_ENOUGH);

		} else {
			return true;
		}
		
	}

	@Override
	/**
	 * check whether amount is valid
	 * 
	 * @param money the amount withdraw form account
	 * @return return true if amount is valid; return false if invalid
	 * @throws ATM_Exception if invalid, print "AMOUNT_INVALID"
	 */
	public boolean isValidAmount(int money) throws ATM_Exception {

		if (money % 1000 != 0) {
			throw new ATM_Exception(ATM_Exception.ExceptionTYPE.AMOUNT_INVALID);
			
		} else {
			return true;
		}
	}

	@Override
	/**
	 * show exception using checkBalance and isValidAmount, and catch exception
	 * 
	 * @param account user account
	 * @param money   withdraw from the account
	 */
	public void withdraw(Account account, int money) {
		try {
			checkBalance(account, money);
			isValidAmount(money);
			int bal = account.getBalance() - money;
			account.setBalance(bal);

		} catch (ATM_Exception e) {
			System.out.println(e.getMessage());

		}
		;
		System.out.println("updated balance : " + account.getBalance());

	}

	public static void main(String args[]) {
		Account David = new Account(4000);
		Simple_ATM_Service atm = new Simple_ATM_Service();
		System.out.println("---- first withdraw ----");
		atm.withdraw(David, 1000);
		System.out.println("---- second withdraw ----");
		atm.withdraw(David, 1000);
		System.out.println("---- third withdraw ----");
		atm.withdraw(David, 1001);
		System.out.println("---- fourth withdraw ----");
		atm.withdraw(David, 4000);
	}

}
