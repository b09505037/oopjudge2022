
public interface ATM_Service {
	/**
	 * check balance in accounts
	 * 
	 * @param account the user's account
	 * @param money   balance in account
	 * @return return true if having enough money; return false if not
	 * @throws ATM_Exception if not having enough money, print "BALANCE_NOT_ENOUGH"
	 */
	public boolean checkBalance(Account account, int money) throws ATM_Exception;

	/**
	 * check whether amount is valid
	 * 
	 * @param money the amount withdraw form account
	 * @return return true if amount is valid; return false if invalid
	 * @throws ATM_Exception if invalid, print "AMOUNT_INVALID"
	 */
	public boolean isValidAmount(int money) throws ATM_Exception;

	/**
	 * show exception using checkBalance and isValidAmount, and catch exception
	 * 
	 * @param account user account
	 * @param money   withdraw from the account
	 */
	public void withdraw(Account account, int money);
}