
public class SimpleArrayList {

	private Integer[] intArray;
	private int size;

	// Default constructor
	public SimpleArrayList() {
		intArray = new Integer[0];
		size = 0;
	}

	// Constructor with initial array size
	public SimpleArrayList(int i) {
		intArray = new Integer[i];
		for (int j = 0; j < i; j++) {
			intArray[j] = 0;
		}
		size = i;
	}

	/**
	 * use increaseSize to increase array size and add an element to it
	 * 
	 * @param i the element we add to the end of the array
	 */
	public void add(Integer i) {
		intArray = increaseSize(intArray);
		intArray[size] = i;
		this.size++;

	}

	/**
	 * increase original array size by one unit
	 * 
	 * @param intArray2 original array
	 * @return an new array that having one more unit than original size
	 */
	public Integer[] increaseSize(Integer[] intArray2) {
		Integer[] tempArray = new Integer[size + 1];
		for (int i = 0; i < size; i++) {
			tempArray[i] = intArray2[i];
		}
		return tempArray;
	}

	/**
	 * returns the element at the specified position in this array
	 * 
	 * @param index the specified position in the array
	 * @return the element at the specified position
	 */
	public Integer get(int index) {
		if (size == 0 || size <= index) {
			return null;
		} else {
			return intArray[index];
		}
	}

	/**
	 * change the element in the specified position and return the original element
	 * 
	 * @param index   the position we want to change in the array
	 * @param element the new element we put in the position
	 * @return the original element
	 */
	public Integer set(int index, Integer element) {
		if (size > index) {
			Integer original_element = intArray[index];
			intArray[index] = element;
			return original_element;
		} else {
			return null;
		}

	}

	/**
	 * remove the element by decreaseSize at the specified position and shift the
	 * subsequent elements to the left if success
	 * 
	 * @param index
	 * @return true: the specified position is not null; false: the specified
	 *         position is null
	 */
	public boolean remove(int index) {
		if (intArray[index] == null)
			return false;
		else {
			intArray = this.decreaseSize(intArray, index);
			this.size--;
			return true;
		}

	}

	/**
	 * decrease the size of the array and shift subsequent elements to the left
	 * 
	 * @param intArray2 the original array
	 * @param index     the position we remove the element
	 * @return the new array we expect
	 */
	public Integer[] decreaseSize(Integer[] intArray2, int index) {
		Integer[] tempArray = new Integer[intArray2.length - 1];
		for (int i = 0; i < index; i++) {
			tempArray[i] = intArray2[i];
		}
		for (int j = index; j < intArray2.length - 1; j++) {
			tempArray[j] = intArray2[j + 1];
		}
		return tempArray;

	}

	/**
	 * clear all the elements in the array
	 */
	public void clear() {
		size = 0;
		Integer[] tempArray = new Integer[size];
		this.intArray = tempArray;
	}

	/**
	 * find the size of the array
	 * 
	 * @return the size of array
	 */
	public int size() {
		return size;
	}

	/**
	 * retains only the elements in this array that are contained in the specified
	 * array
	 * 
	 * @param l the specified array
	 * @return true: if array change; false: if there is no change in array
	 */
	public boolean retainAll(SimpleArrayList l) {
		Integer[] tempArray = new Integer[size];
		for (int i = 0; i < this.size; i++) {
			for (int j = 0; j < l.size; j++) {
				if (this.intArray[i] == l.intArray[j]) {
					tempArray[i] = intArray[i];
					break;
				}
			}
		}
		int new_size = size; // find the new size in new array
		for (int i = 0; i < size; i++) {
			if (tempArray[i] == null) {
				new_size = new_size - 1;
			}
		}
		// delete all nulls in the array
		for (int i = 0; i < size; i++) {
			if (tempArray[i] == null) {
				for (int j = i; j < size - 1; j++) {
					tempArray[j] = tempArray[j + 1];
				}
			}
		}
		// form a newArray with new_size
		Integer[] newArray = new Integer[new_size];
		for (int i = 0; i < new_size; i++) {
			newArray[i] = tempArray[i];
		}

		this.intArray = newArray;

		// check whether the array change or not
		if (new_size != size) {
			size = new_size;
			return true;
		} else {
			return false;
		}

	}

	public static void main(String args[]) {
		System.out.println("=== TASK 1 ===");
		SimpleArrayList list = new SimpleArrayList();
		System.out.println(list.get(0));

		System.out.println("=== TASK 2 ===");
		list.add(2);
		list.add(5);
		list.add(8);
		list.add(1);
		list.add(12);
		System.out.println(list.get(2));

		System.out.println("=== TASK 3 ===");
		System.out.println(list.get(5));

		System.out.println("=== TASK 4 ===");
		System.out.println(list.set(2, 100));

		System.out.println("=== TASK 5 ===");
		System.out.println(list.get(2));

		System.out.println("=== TASK 6 ===");
		System.out.println(list.set(5, 100));

		System.out.println("=== TASK 7 ===");
		System.out.println(list.remove(2));

		System.out.println("=== TASK 8 ===");
		System.out.println(list.get(2));

		System.out.println("=== TASK 9 ===");
		System.out.println(list.remove(2));

		System.out.println("=== TASK 10 ===");
		System.out.println(list.get(2));

		System.out.println("=== TASK 11 ===");
		System.out.println(list.get(3));

		System.out.println("=== TASK 12 ===");
		list.clear();
		System.out.println(list.get(0));

		System.out.println("=== TASK 13 ===");
		SimpleArrayList list2 = new SimpleArrayList(5);
		System.out.println(list2.get(3));

		System.out.println("=== TASK 14 ===");
		System.out.println(list2.get(9));

		System.out.println("=== TASK 15 ===");
		for (int i = 0; i < list2.size(); i++) {
			System.out.println(list2.set(i, i));
		}
		for (int i = 0; i < 5; i++) {
			list.add(i);
		}
		System.out.println(list.retainAll(list2));

		System.out.println("=== TASK 16 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 17 ===");
		System.out.println(list2.remove(0));
		System.out.println(list2.remove(2));
		System.out.println(list.retainAll(list2));

		System.out.println("=== TASK 18 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 19 ===");
		System.out.println(list.set(1, null));
		System.out.println(list.remove(1));

		System.out.println("=== TASK 20 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 21 ===");
		System.out.println(list.set(1, 123));

		System.out.println("=== TASK 22 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 23 ===");
		System.out.println(list.remove(1));

		System.out.println("=== TASK 24 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 25 ===");
		list.add(null);
		System.out.println(list.remove(2));

		System.out.println("=== TASK 26 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
	}

}
