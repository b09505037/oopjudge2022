import java.util.*;
public class SentenceProcessor {
	public static void main(String args[]) {
		SentenceProcessor sp = new SentenceProcessor();

		System.out.println(sp.removeDuplicatedWords("Hello Hello World I love love the World I lovelove the World"));
		System.out.println(sp.removeDuplicatedWords("Buffalo buffalo Buffalo buffalo buffalo buffalo Buffalo buffalo"));
		System.out.println(sp.removeDuplicatedWords("a a la a la carte A la La carte Carte A a la la"));
		System.out.println(sp.replaceWord("major", "minor", "The major problem is how to sing in A major"));
		System.out.println(sp.replaceWord("on", "off", "Turn on the television I want to keep the television on"));
		System.out.println(sp.replaceWord("love", "hate", "I love the World I lovelove the Love"));
        }

	/**
	 * 
	 * @param sentence 包含句子
	 *
	 * @return 將字串以string
	 */
	public String removeDuplicatedWords(String sentence) {
		HashSet<String> token_set;
		StringBuffer result;//result 句子重組結果StringBuffer可多次修改
		StringBuffer token; //token 標點符號及空格
		String tok; //標點符號和空格
        int character, length;	//character 標點符號和空格 | length 計算句子長度
 
        token_set = new HashSet<String>();
        result = new StringBuffer();
        token = new StringBuffer();
        for (int i = 0; i < sentence.length(); ++i) {
        	character = sentence.charAt(i);
        	if (character == ' ' || character == ',' || character == '.') {
        		tok = token.toString();
        		
        		if (token_set.contains(tok)) {
        			if ((length = result.length()) != 0 && result.charAt(length - 1) == ' ')
        				result.deleteCharAt(length - 1);
        		}
        		else {
        			token_set.add(tok);
                    result.append(tok);
        		}
        		token = new StringBuffer();
        		result.append((char)character);
        	}
        	else {
        		token.append((char)character);
        		}
        }
                tok = token.toString();
                if (token_set.contains(tok)) {
                	if ((length = result.length()) != 0 && result.charAt(length - 1) == ' ')
                		result.deleteCharAt(length - 1);
                	}
                else {
                	token_set.add(tok);
                	result.append(tok);
                }
                return result.toString();
	}
	/**
	 * 
	 * @param target 目標取代
	 * @param replacement 取代字
	 * @param sentence 原始句子
	 * @return 新句子
	 */
	public String replaceWord(String target, String replacement, String sentence) {
		{
			//取代特定詞
		return sentence.replaceAll("\\b" + target + "\\b", replacement);
		}
	}
}
		
		
	