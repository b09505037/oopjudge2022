/**IsLeapYear: check the year is a leap year or not
 * 
 * @author LIN,YI-SHENG
 * 
 */
public class IsLeapYear {
	/**
	 * 
	 * @param year
	 * @return if it is leap year,return "true", else return "false"
	 * 
	 */
	public static boolean determine(int a)
	{	
		int year = a;
		if ((year % 4 == 0) && year % 100 != 0)
            return true;
		// 可被4整除、但不能被100
        else if ((year % 4 == 0) && (year % 100 == 0) && (year % 400 == 0))
            return true;
		// 可同時被4、100、400
        else
        	return false;
		//其餘皆非閏年
	}	
}
