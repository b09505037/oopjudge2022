//計算特定天數後green_crud
public class GreenCrud {
	/**
	 * 
	 * @param a 初始green＿crud
	 * @param b 求取數量的目標天數
	 * @param times 經過幾次的成長
	 * @param finalSize 目標天數時的數目
	 * @return return the final size
	 */
	public static int calPopulation(int a,int b) {
		int initialSize = a, times = b/5, finalSize = a;
		int f1, f2;
		//一次循環，final=initial 
		if (times == 1) {
			finalSize = a ;
		}
		//參照 Fibonacci sequence
		else {
			f1 = initialSize;
			f2 = initialSize;
			for(int i = 2; i <= times; i++) {
				finalSize = f1+f2;
				f1 = f2;
				f2 = finalSize;
			
			}
		}
		return finalSize;
		
	}
	
}
