/**
 * store information about an order of pizza
 * @author linyisheng
 *
 */
public class PizzaOrder {
	private int num_of_pizza;					//the number of pizza in the order
	private double cost[] = {0.0, 0.0, 0.0};	//store the price of pizza  
	
	/**
	 * check the number of pizza between 1 and 3 
	 * @param numberPizzas number of pizza in this order 
	 * @return whether numberPizzas is reasonable
	 */
	public boolean setNumberPizzas(int numberPizzas) {
		if (numberPizzas < 1 ||  numberPizzas > 3)
			return false;
		else
			num_of_pizza = numberPizzas;
			return true;
	}
	/**
	 * find out the price of the first pizza 
	 * @param pizza1 the first pizza
	 */
	public void setPizza1(Pizza pizza1) {
		cost[0] = pizza1.calcCost();

	}
	/**
	 * find out the price of the second pizza 
	 * @param pizza2 the second pizza
	 */
	public void setPizza2(Pizza pizza2) {
		cost[1] = pizza2.calcCost();
	}
	/**
	 * find out the price of the third pizza 
	 * @param pizza3 the third pizza
	 */
	public void setPizza3(Pizza pizza3) {
		cost[2] = pizza3.calcCost();
	}
	/**
	 * use array to calculate the total price 
	 * @return the cost of the order
	 */
	public double calcTotal() {
		double sum = 0;
		for(int i = 0; i < num_of_pizza; i++) {
			sum += cost[i]; 
		}
		return sum;
	}
	
	public static void main(String args[]) {
		Pizza pizza1 = new Pizza("large", 1, 0, 1);
		Pizza pizza2 = new Pizza("medium", 2, 2, 5);
		Pizza pizza3 = new Pizza();
		PizzaOrder order = new PizzaOrder();
		System.out.println(order.setNumberPizzas(5));
		order.setNumberPizzas(2);
		order.setPizza1(pizza1);
		order.setPizza2(pizza2);
		System.out.println(order.calcTotal());
		order.setNumberPizzas(3);
		order.setPizza1(pizza1);
		order.setPizza2(pizza2);
		order.setPizza3(pizza3);
		System.out.println(order.calcTotal());
	}
}


	


	
	

