/**
 * store information about pizza
 * @author linyisheng
 *
 */
public class Pizza {
	
	private String size;	//pizza size
	private int num_c;		//number of cheese
	private int num_p;		//number of pepperoni
	private int num_h;		//number of ham
	
	/**
	 * default constructor
	 *
	 */
	public Pizza() {
		//default constructor
		size = "small";
		num_c = 1;
		num_p = 1;
		num_h = 1;
	}
	/**
	 * constructor
	 * 
	 * @param str size of pizza
	 * @param c number of cheese
	 * @param p number of pepperoni
	 * @param h number of ham
	 */
	public Pizza(String str, int c, int p, int h) {
		size = str;
		num_c = c;
		num_p = p;
		num_h = h;
	}
	
	/**
	 * 設定pizza
	 * @param str pizza size
	 */
	public void setSize(String str) {
		
		this.size = str;
	}
	/**
	 * 設定cheese數量
	 * @param n number of cheese
	 */
	public void setNumberOfCheese(int n) {
		this.num_c = n;
	}
	/**
	 * 設定pepperoni數量
	 * @param n number of pepperoni
	 */
	public void setNumberOfPepperoni(int n) {
		this.num_p = n;
	}
	/**
	 * 設定ham數量
	 * @param n number of ham
	 */
	public void setNumberOfHam(int n) {
		this.num_h = n;
	}
	
	/**
	 * 取得pizza大小
	 * @return pizza size
	 */
	public String getSize() {
		return size;
	}
	/**
	 * 取得cheese數量
	 * @return number of cheese
	 */
	public int getNumberOfCheese() {
		return num_c;
	}
	/**
	 * 取得pepperoni數量
	 * @return number of pepperoni
	 */
	public int getNumberOfPepperoni() {
		return num_p;
	}
	/**
	 * 取得ham數量
	 * @return number of ham
	 */
	public int getNumberOfHam() {
		return num_h;
	}
	/**
	 * 計算總金額
	 * @return total cost
	 */
	public double calcCost() {
		double sum = 0;
		
		if (size == "small")
				sum += 10;
		else if (size == "medium")
				sum += 12;
		else if (size == "large")
				sum += 14;
		
		sum += 2*num_c;
		sum += 2*num_p;
		sum += 2*num_h;
		
		return sum;
		
		
	}
	
	/**
	 * 把資訊以string
	 */
	public String toString() {
		String str = "size = "+size+", numOfCheese = "+num_c+", numOfPepperoni = "+num_p+", numOfHam = "+num_h;
		return str;
	}
	/**
	 * 比較兩個pizza是否相同
	 * @param obj 比較pizza
	 * @return whether this pizza is the same as the other
	 */
	public boolean equals(Pizza obj) {
		if(this.size == obj.size && this.num_c == obj.num_c && this.num_p == obj.num_p && this.num_h == obj.num_h)
			return true;
		else 
			return false;
		
	}
	
	public static void main(String args[]) {
		Pizza pizza = new Pizza("large", 3, 1, 5);
		System.out.println(pizza.getSize());
		System.out.println(pizza.getNumberOfCheese());
		System.out.println(pizza.getNumberOfPepperoni());
		System.out.println(pizza.getNumberOfHam());
		pizza = new Pizza();
		pizza.setSize("medium");
		pizza.setNumberOfCheese(2);
		pizza.setNumberOfPepperoni(4);
		pizza.setNumberOfHam(1);
		System.out.println(pizza.getSize());
		System.out.println(pizza.getNumberOfCheese());
		System.out.println(pizza.getNumberOfPepperoni());
		System.out.println(pizza.getNumberOfHam());
		System.out.println(pizza.calcCost());
		System.out.println(pizza.toString());
		System.out.println(pizza.equals(new Pizza("large", 2, 4, 1)));
		System.out.println(pizza.equals(new Pizza()));
		System.out.println(pizza.equals(new Pizza("medium", 2, 4, 1)));
	}
	

}
