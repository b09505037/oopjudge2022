
class UnknownCmdException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
/**
 * find the error message
 * @param errMessage one of the five kinds of problems.
 */
	public UnknownCmdException(String errMessage) {
		super(errMessage);
		
	}
}
