
/**
 * simple calculator
 * @author linyisheng
 *
 */
import java.text.DecimalFormat;

public class SimpleCalculator {
	
	private Double answer; // the answer after calculation
	private char operator;
	private Double value;
	private String result; // the result that show after calculation
	private int count; // times of calculation
	boolean end; // check whether the commands are finished

	public SimpleCalculator() {
		answer = 0.00;
		count = 0;
	}

	/**
	 * check errors and calculate the answer
	 * 
	 * @param cmd the command
	 * @throws UnknownCmdException show which kind of errors
	 */
	public void calResult(String cmd) throws UnknownCmdException {
		boolean validOperator = true; // operator status
		boolean validValue = true;// value status
		if (cmd != "R" || cmd != "r") {
			if (cmd.split(" ").length != 2) {
				// check the form is correct or not
				throw new UnknownCmdException("Please enter 1 operator and 1 value separated by 1 space");
			} else {
				operator = cmd.charAt(0);
				if (operator != '+' && operator != '-' && operator != '*' && operator != '/') {
					// check operator valid
					validOperator = false;
				}

				try {
					// check value valid
					value = Double.valueOf(cmd.substring(2));
				} catch (Exception e) {
					validValue = false;
				}

				if ((operator == '/') && (value == 0)) {
					throw new UnknownCmdException("Can not divide by 0");
				}
				if (validOperator == false && validValue == false) {
					// both invalid
					if (cmd.charAt(1) == ' ') {
						throw new UnknownCmdException(
								operator + " is an unknown operator and " + cmd.charAt(2) + " is an unknown value");
					}
				}
				if (validOperator == true && validValue == false) {
					// invalid value
					throw new UnknownCmdException(cmd.charAt(2) + " is an unknown value");

				}
				if (validOperator == false && validValue == true) {
					// invalid operator
					throw new UnknownCmdException(operator + " is an unknown operator");

				}
				if (validOperator == true && validValue == true) {
					// calculate the answer
					switch (operator) {
					case '+':
						answer += value;
						break;
					case '-':
						answer -= value;
						break;
					case '*':
						answer *= value;
						break;
					case '/':
						answer /= value;
						break;
					}
				}
			}
			count++;
		}

	}

	/**
	 * get the message about the process and result
	 * 
	 * @return process and result
	 */
	public String getMsg() {
		DecimalFormat df = new DecimalFormat("0.00");// format the form of the number
		if (end == true) {
			result = "Final result = " + df.format(answer);
		} else if (count == 0) {
			result = "Calculator is on. Result = " + df.format(answer);
		} else if (count == 1) {
			result = "Result " + operator + " " + df.format(value) + " = " + df.format(answer) + ". New result = "
					+ df.format(answer);
		} else {
			result = "Result " + operator + " " + df.format(value) + " = " + df.format(answer) + ". Updated result = "
					+ df.format(answer);
		}
		return result;

	}

	/**
	 * check the calculation is end or not
	 * 
	 * @param cmd command
	 * @return whether it is end or not
	 */
	public boolean endCalc(String cmd) {
		if ("R".equals(cmd) || "r".equals(cmd)) {
			end = true;
		} else
			end = false;

		return end;
	}

	public static void main(String[] args) {
		SimpleCalculator cal = new SimpleCalculator();
		System.out.println(cal.getMsg());
		String cmd_str = "+ 5,- 2,* 5,/ 3,% 2,* D,X D,XD,, ,/ 1000000,/ 00.000,/ 0.000001,+ 1 + 1,- 1.66633,r R,r";
		String[] cmd_arr = cmd_str.split(",");
		for (int i = 0; i < cmd_arr.length; i++) {
			try {
				if (cal.endCalc(cmd_arr[i]))
					break;
				cal.calResult(cmd_arr[i]);
				System.out.println(cal.getMsg());
			} catch (UnknownCmdException e) {
				System.out.println(e.getMessage());
			}
		}
		System.out.println(cal.getMsg());
	}

}
